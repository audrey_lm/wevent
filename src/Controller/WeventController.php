<?php

// src/Controller/LuckyController.php
namespace App\Controller;

use App\Entity\User;
use App\Entity\Evenement;
use App\Entity\Invitation;
use App\Form\EvenementType;
use App\Entity\UserEvenement;
use App\Form\InscriptionType;
use App\Form\ModifyProfileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class WeventController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render('home.html.twig');
    }
    /**
     * @Route("/notification", name="notification")
     */
    public function notification()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $invitations = $this->getDoctrine()
                            ->getRepository(Invitation::class)
                            ->findBy(['createur'=>$this->getUser()]);
        
        return $this->render('/site/notification.html.twig', [
            'user'          => $this->getUser(),
            'invitations'   => $invitations,
            'nb_invit'      => $this->countInvitations()   
        ]);

    }
    /**
     * @Route("/calendrier", name="calendrier")
     */
    public function calendrier()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user       = $this->getUser();
        $evenements = $this->getDoctrine()
                           ->getRepository(UserEvenement::class)
                           ->findBy(array('user' => $user->getId()));
        

        return $this->render('/site/calendrier.html.twig', [
            'evenements'    => $evenements,
            'user'          => $user,
            'nb_invit'      => $this->countInvitations()
        ]);
    }

    /**
     * @Route("/amis", name="amis")
     */
    public function amis()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $users  = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('/site/amis.html.twig', [
            'user'      => $this->getUser(),
            'users'     => $users,
            'nb_invit'  => $this->countInvitations()
        ]);
    }
     /**
     * @Route("/mesEvenementsAVenir", name="aVenir")
     */
    public function listeEvenementsAVenir()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $repoEve=$this->getDoctrine()->getRepository(UserEvenement::class);
        $evenements=$repoEve->findEvenementAVenir($user,new \Datetime());
        return $this->render('/site/evenements.html.twig',[
            'evenements' => $evenements,
            'user' => $user,
            'titre'=>'événements futurs',
            'nb_invit'=>$this->countInvitations()
        ]);
    }
    /**
     * @Route("/mesEvenementsPasses", name="Passé")
     */
    public function listeEvenementsPasse()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $repoEve=$this->getDoctrine()->getRepository(UserEvenement::class);
        $evenements=$repoEve->findEvenementPasse($user,new \Datetime());
        return $this->render('/site/evenements.html.twig',[
            'evenements' => $evenements,
            'user' => $user,
            'titre'=>'événements passés',
            'nb_invit'=>$this->countInvitations()
        ]);
    }
    
        

    /**
     * @Route("/evenement/{id}", name="show_evenement")
     */
    public function showEvenement(int $id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        //if permission(invité à l'événement ou autre)

        $user           = $this->getUser();
        $userEvenement  = $user->getUserEvenements()->toArray();

        foreach ($userEvenement as $ue) 
        {
            if ($ue->getEvenement()->getId() == $id) 
            {
                $evenement  = $this->getDoctrine()
                                ->getRepository(Evenement::class)
                                ->find($id);

                return $this->render('/site/evenementShow.html.twig', [
                    'evenement' => $evenement,
                    'user'      => $user,
                    'nb_invit'  => $this->countInvitations()
                ]);
            }
        }
        //sinon return une page qui dit qu'on a pas été invité à cette événement.

        return $this->render('/site/calendrier.html.twig', [
            'user'      => $user,
            'nb_invit'  => $this->countInvitations()
        ]);
    }

    /**
     * @Route("/evenement-new", name="new_evenement", methods={"GET","POST"})
     */
    public function newEvenement(Request $request): Response
    {
        $user       = $this->getUser();
        $evenement  = new Evenement();
        $form       = $this->createForm(EvenementType::class, $evenement);
        $submittedToken = $request->request->get('_csrf_token');
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()&& $this->isCsrfTokenValid('nouvelEvenement', $submittedToken)) 
        {
            $em = $this->getDoctrine()->getManager();
            if(isset($_POST['invitations'])){
                
                $invites = $_POST['invitations'];

                foreach($invites as $id ){

                    $invitation = new Invitation();
                    $invitation->setCreateur($user);
                    $invitation->setDestinataire($em->getRepository(User::class)->find($id));
                    $invitation->setEvenement($evenement);
                    $invitation->setEtat('En attente');

                    $em->persist($invitation);

                }
            }

            $userEvenement = new UserEvenement();
            $userEvenement->setUser($user);
            $userEvenement->setEvenement($evenement);
            $userEvenement->setRole("Créateur");

            $em->persist($userEvenement);
            $em->persist($evenement);

            $em->flush();

            $this->addFlash('success', 'L\'événement a bien été créé !');
            return $this->redirectToRoute('show_evenement', [
                'id'            => $evenement->getId(), 
                'nom'           => $evenement->getNom(),
                'nb_invit'      => $this->countInvitations()
            ]);
        }

        return $this->render('site/evenementNew.html.twig', [
            'evenement' => $evenement,
            'form'      => $form->createView(),
            'user'      => $user,
            'nb_invit'  => $this->countInvitations()
        ]);
    }


    /**
     * @Route("/evenement-edit", name="edit_evenement")
     * @todo Dvlper la vue modif
     */
    public function editEvenement()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('/site/evenementEdit.html.twig', [
            'user'      => $this->getUser(),
            'nb_invit'  => $this->countInvitations()
        ]);
    }


    /**
     * @Route("/profil", name="profil")
     */
    public function profil(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $erreur=null;
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $submittedToken = $request->request->get('_csrf_token');
            $user = $this->getUser();
            $form=$this->createForm(ModifyProfileType::class);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()&& $this->isCsrfTokenValid('profil', $submittedToken)){
                $data=  $form->getData();
                if($data['Email']==$user->getEmail()){          
                    $user->setPrenom($data['Prenom']);
                    $user->setNom($data['Nom']);
                    $user->setEmail($data['Email']);
                    $manager=$this->getDoctrine()->getManager();
                    $manager->persist($user);
                    $manager->flush();
                    $this->addFlash('success', 'Votre profil a bien été mis à jour !');
                    return $this->redirectToRoute('profil');
                }
                if($this->getDoctrine()->getRepository(User::class)->findBy($data['Email'])!=null)
                    $erreur="Attention cette adresse email est déjà utilisé, vous ne pouvez donc l'utiliser.";
                else {
                    $user->setPrenom($data['Prenom']);
                    $user->setNom($data['Nom']);
                    $user->setEmail($data['Email']);
                    $manager=$this->getDoctrine()->getManager();
                    $manager->persist($user);
                    $manager->flush();
                    $this->addFlash('success', 'Votre profil a bien été mis à jour !');
                    return $this->redirectToRoute('profil');
                }
            }
            return $this->render('/site/profil.html.twig',[
                'user' => $user,
                'form'=>$form->createView(),
                'erreur'=>$erreur,
                'nb_invit'  => $this->countInvitations()
            ]);
    }


    /**
     * @Route("/toutlemondeamis", name="toutlemondeamis")
     * fonction momentanée
     */
    public function toutlemondeamis()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $users  = $this->getDoctrine()->getRepository(User::class)->findAll();

        foreach($users as $ami){
            if($user->getId()!=$ami->getId()){
                $user->addAmi($ami);
            }
        }

        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('amis');
    }

    /**
     *
     * @return integer nombre d'invitation sans réponse de l'utilisateur
     */
    private function countInvitations() : int
    {  

        return count($this
            ->getDoctrine()
            ->getRepository(Invitation::class)
            ->findBy(['destinataire'=>$this->getUser(), 'etat'=>'En attente']));
    }

     /**
     * @Route("/invitations", name="liste_invitations")
     */
    public function invitations(){

        $invitations = $this->getDoctrine()
                            ->getRepository(Invitation::class)
                            ->findBy(['destinataire'=>$this->getUser(), 'etat'=>'En attente']);
        
        return $this->render('site/invitations.html.twig', [
            'user'          => $this->getUser(),
            'invitations'   => $invitations,
            'nb_invit'      => $this->countInvitations()
        ]);
    }

    /**
     * @Route("/accepter/{id}", name="accepterInvit")
     */
    public function accepter($id){
        
        $em = $this->getDoctrine()->getManager();
        $invitation = $this->getDoctrine()
            ->getRepository(invitation::class)
            ->find($id);
        
        $invitation->setEtat("Acceptée");
        $em->persist($invitation);
        $em->flush();

        return $this->redirectToRoute('liste_invitations');
    }

    /**
     * @Route("/refuser/{id}", name="refuserInvit")
     */
    public function refuser($id){

        $em = $this->getDoctrine()->getManager();
        $invitation = $this->getDoctrine()
            ->getRepository(invitation::class)
            ->find($id);

        $invitation->setEtat("Refusée");
        $em->persist($invitation);
        $em->flush();

        return $this->redirectToRoute('liste_invitations');
       
    }

    /**
     * @Route("/delete-invitation/{id}", name="deleteInvit")
     */
    public function deleteInvit($id){

        $em = $this->getDoctrine()->getManager();
        $invitation = $this->getDoctrine()->getRepository(invitation::class)->find($id);
        
        if($invitation->getEtat() == 'Acceptée')
        {
            $userEvenement = new UserEvenement();
            $userEvenement->setUser($invitation->getDestinataire());
            $userEvenement->setEvenement($invitation->getEvenement());
            $userEvenement->setRole('Invité');

            $em->persist($userEvenement);
        }

        $em->remove($invitation);
        $em->flush();

        return $this->redirectToRoute('calendrier');
       
    }
}

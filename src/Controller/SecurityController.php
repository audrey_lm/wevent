<?php

namespace App\Controller;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Evenement;
use App\Entity\UserEvenement;
use App\Form\InscriptionType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscription(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user=new User();
        $form=$this->createForm(InscriptionType::class,$user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $faker= \Faker\Factory::create('fr_FR');
            $hash=$encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager=$this->getDoctrine()->getManager();
            for($j=1 ; $j<= mt_rand(5, 10);$j++){
                $evenement=new Evenement();
                $userEvenement=new UserEvenement();
                $userEvenement->setRole('Créateur');
                $evenement->setNom($faker->sentence);
                $evenement->setTheme('fêtes');
                $description='<p>'.join($faker->paragraphs(6), '</p><p>').'</p>';
                $evenement->setDescription($description);
                $evenement->setAdresse($faker->address);
                $evenement->setCodePostal($faker->postcode);
                $evenement->setVille($faker->city);
                $evenement->setDateDebut(new \DateTime());
                $date=$evenement->getDateDebut()->add(new \DateInterval('P'.$j.'M'));
                $evenement->setDateDebut($date);
                $userEvenement->setUser($user);
                $userEvenement->setEvenement($evenement);
                $manager->persist($userEvenement);
                $manager->persist($evenement);
           }
           for($j=1 ; $j<= mt_rand(5, 10);$j++){
            $evenement=new Evenement();
            $userEvenement=new UserEvenement();
            $userEvenement->setRole('Créateur');
            $evenement->setNom($faker->sentence);
            $evenement->setTheme('fêtes');
            $description='<p>'.join($faker->paragraphs(6), '</p><p>').'</p>';
            $evenement->setDescription($description);
            $evenement->setAdresse($faker->address);
            $evenement->setCodePostal($faker->postcode);
            $evenement->setVille($faker->city);
            $evenement->setDateDebut(new \DateTime());
            $date=$evenement->getDateDebut()->sub(new \DateInterval('P'.$j.'M'));
            $evenement->setDateDebut($date);
            $userEvenement->setUser($user);
            $userEvenement->setEvenement($evenement);
            $manager->persist($userEvenement);
            $manager->persist($evenement);
       }
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('login');
        }
        return $this->render('security/inscription.html.twig', ['form'=> $form->createView() ]);
    }
    /**
     * @Route("/login", name="login")
     */
    public function login()
    {
        
        return $this->render('security/login.html.twig');
        
    }
    /**
     * @Route("logout", name="logout");
     */
    public function logout(){}
}

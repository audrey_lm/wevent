<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Evenement;
use App\Entity\UserEvenement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EvenementsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker= \Faker\Factory::create('fr_FR');
       for($i=1;$i<=7;$i++){
           $user=new User();
           $user->setEmail($faker->email);
           $user->setPassword('123456789');
           $user->setConfirmPassword('123456789');
           $user->setNom($faker->name);
           $user->setPrenom($faker->firstname);
           $manager->persist($user);
           for($j=1 ; $j<= mt_rand(5, 10);$j++){
                $evenement=new Evenement();
                $userEvenement=new UserEvenement();
                $userEvenement->setRole('Créateur');
                $evenement->setNom($faker->title);
                $evenement->setTheme('fêtes');
                $description='<p>'.join($faker->paragraphs(6), '</p><p>').'</p>';
                $evenement->setDescription($description);
                $evenement->setLieu($faker->address);
                $evenement->setDate(new \DateTime());
                $date=$evenement->getDate()->add(new \DateInterval('P'.$i.'M'));
                $evenement->setDate($date);
                $userEvenement->setUser($user);
                $userEvenement->setEvenement($evenement);
                $manager->persist($userEvenement);
                $manager->persist($evenement);
           }
       }
           
        $manager->flush();
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200104184334 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE evenement ADD code_postal INT NOT NULL, ADD ville VARCHAR(255) NOT NULL, ADD date_debut DATETIME NOT NULL, ADD filename VARCHAR(255) DEFAULT NULL, CHANGE lieu adresse VARCHAR(255) NOT NULL, CHANGE date date_fin DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE amis ADD CONSTRAINT FK_9FE2E7613AD8644E FOREIGN KEY (user_source) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE amis ADD CONSTRAINT FK_9FE2E761233D34C1 FOREIGN KEY (user_target) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE amis DROP FOREIGN KEY FK_9FE2E7613AD8644E');
        $this->addSql('ALTER TABLE amis DROP FOREIGN KEY FK_9FE2E761233D34C1');
        $this->addSql('ALTER TABLE evenement ADD lieu VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP adresse, DROP code_postal, DROP ville, DROP date_debut, DROP filename, CHANGE date_fin date DATETIME DEFAULT NULL');
    }
}

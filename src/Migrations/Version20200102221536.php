<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200102221536 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_evenement DROP FOREIGN KEY FK_BC6E5FA63C02CD4');
        $this->addSql('ALTER TABLE user_evenement DROP FOREIGN KEY FK_BC6E5FA67B3B43D');
        $this->addSql('DROP INDEX IDX_BC6E5FA67B3B43D ON user_evenement');
        $this->addSql('DROP INDEX IDX_BC6E5FA63C02CD4 ON user_evenement');
        $this->addSql('ALTER TABLE user_evenement ADD user_id INT NOT NULL, ADD evenement_id INT NOT NULL, DROP users_id, DROP evenements_id');
        $this->addSql('ALTER TABLE user_evenement ADD CONSTRAINT FK_BC6E5FAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_evenement ADD CONSTRAINT FK_BC6E5FAFD02F13 FOREIGN KEY (evenement_id) REFERENCES evenement (id)');
        $this->addSql('CREATE INDEX IDX_BC6E5FAA76ED395 ON user_evenement (user_id)');
        $this->addSql('CREATE INDEX IDX_BC6E5FAFD02F13 ON user_evenement (evenement_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_evenement DROP FOREIGN KEY FK_BC6E5FAA76ED395');
        $this->addSql('ALTER TABLE user_evenement DROP FOREIGN KEY FK_BC6E5FAFD02F13');
        $this->addSql('DROP INDEX IDX_BC6E5FAA76ED395 ON user_evenement');
        $this->addSql('DROP INDEX IDX_BC6E5FAFD02F13 ON user_evenement');
        $this->addSql('ALTER TABLE user_evenement ADD users_id INT NOT NULL, ADD evenements_id INT NOT NULL, DROP user_id, DROP evenement_id');
        $this->addSql('ALTER TABLE user_evenement ADD CONSTRAINT FK_BC6E5FA63C02CD4 FOREIGN KEY (evenements_id) REFERENCES evenement (id)');
        $this->addSql('ALTER TABLE user_evenement ADD CONSTRAINT FK_BC6E5FA67B3B43D FOREIGN KEY (users_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_BC6E5FA67B3B43D ON user_evenement (users_id)');
        $this->addSql('CREATE INDEX IDX_BC6E5FA63C02CD4 ON user_evenement (evenements_id)');
    }
}

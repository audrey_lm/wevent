<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *  fields= {"email"},
 *  message= "L'email que vous avez indiqué est déjà utiliser."
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8", minMessage="Votre message doit faire au minimum 8 caractères")
     * @Assert\EqualTo(propertyPath="confirm_password", message="Le mot de passe est différent")
     */
    private $password;
    /**
     * @Assert\EqualTo(propertyPath="password", message="Le mot de passe est différent.")
     */
    private $confirm_password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserEvenement", mappedBy="user")
     */
    private $userEvenements;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     * @ORM\JoinTable(name="amis")
     */
    private $amis;
    
    protected $captchaCode;
    

    public function __construct()
    {
        $this->userEvenements = new ArrayCollection();
        $this->amis = new ArrayCollection();
    }

    public function getConfirmPassword(): ?string
    {
        return $this->confirm_password;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    public function setConfirmPassword(string $confirm_password): self
    {
        $this->confirm_password = $confirm_password;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUsername(){
        return $this->email;
    }
    public function eraseCredentials(){}
    public function getSalt(){}
    public function getRoles(){
        return['ROLE_USER'];
    }
    
    public function getCaptchaCode()
    {
      return $this->captchaCode;
    }

    public function setCaptchaCode($captchaCode)
    {
      $this->captchaCode = $captchaCode;
    }

    /**
     * @return Collection|UserEvenement[]
     */
    public function getUserEvenements(): Collection
    {
        return $this->userEvenements;
    }

    public function addUserEvenement(UserEvenement $userEvenement): self
    {
        if (!$this->userEvenements->contains($userEvenement)) {
            $this->userEvenements[] = $userEvenement;
            $userEvenement->setUser($this);
        }

        return $this;
    }

    public function removeUserEvenement(UserEvenement $userEvenement): self
    {
        if ($this->userEvenements->contains($userEvenement)) {
            $this->userEvenements->removeElement($userEvenement);
            // set the owning side to null (unless already changed)
            if ($userEvenement->getUser() === $this) {
                $userEvenement->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAmis(): Collection
    {
        return $this->amis;
    }

    public function addAmi(User $ami): self
    {
        if (!$this->amis->contains($ami)) {
            $this->amis[] = $ami;
        }

        return $this;
    }

    public function removeAmi(User $ami): self
    {
        if ($this->amis->contains($ami)) {
            $this->amis->removeElement($ami);
        }

        return $this;
    }
}
